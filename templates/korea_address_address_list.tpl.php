<?php
/**
 * Created by PhpStorm.
 * User: pig482
 * Date: 2016. 11. 24.
 * Time: PM 6:27
 */
$address_base = $variables['address'];
$return_id = $variables['return_id'];
$address = $address_base->sido;
$address .= !empty( $address_base->sigugun ) ? ' ' . $address_base->sigugun : '';
$address .= ' ' . $address_base->load_name;
$address .=  ' ' . $address_base->building_mast_no;
$address .= ( !empty( $address_base->building_slave_no) && $address_base->building_slave_no != 0 ) ? '-' . $address_base->building_slave_no : '';
$dong_name = $address_base->law_dong_name;
$building_name = $address_base->building_name;
$address .= !empty( $building_name ) ? ' ' . $building_name : '';
?>
<div class="stv_korea_address_list_item_wrapper">
    <div class="stv_korea_address_zipcode"><?=$address_base->zipcode?></div>
    <div class="stv_korea_address_address"><?=( $address . ' ' . $address_slave )?></div>
    <div class="stv_korea_address_link">
        <a href="javascript:void Drupal.behaviors.updateKoreaAddressAtComponent( '<?=$return_id?>', { zipcode: '<?=$address_base->zipcode?>', address: '<?=$address?>', dong_name: '<?=$dong_name?>', building_name: '<?=$building_name?>' } )" title="<?=( t( 'Select' ) . ' ' . $address )?>">
        <?=t( 'Select' )?>
        </a>
    </div>
</div>
