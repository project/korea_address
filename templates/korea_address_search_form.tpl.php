<?php
/**
 * Created by PhpStorm.
 * User: pig482
 * Date: 2016. 11. 24.
 * Time: PM 1:54
 */
?>
<div class="stv_korea_address_search_form_header">
    <h2><?=t( 'Search Address' )?></h2>
</div>
<div class="stv_korea_address_form_wrapper">
    <form>
        <select id="stv_korea_address_search_sido">
            <option value=""><?=t( 'Select sido' )?></option>
            <?php foreach( $variables['sido'] as $key => $obj ): ?>
            <option value="<?=$key?>"><?=$obj->sido_ko_name?></option>
            <?php endforeach; ?>
        </select>
        <select id="stv_korea_address_sigugun">
            <option value=""><?=t( 'Select SIGUGUN' )?></option>
        </select>
        <input type="text" placeholder="<?=t( 'Enter Load name.' )?>" id="stv_korea_address_load_name"/>
        <input data-component_id="<?=$variables['return_id']?>" type="button" value="<?=t( 'Search' )?>" id="stv_korea_address_search_submit"/>
    </form>
</div>
<div class="stv_korea_address_list_wrapper">
    <div class="stv_korea_address_list_header">
        <div class="stv_korea_address_zipcode"><?=t( 'Zip Code' )?></div>
        <div class="stv_korea_address_address"><?=t( 'Address' )?></div>
        <div class="stv_korea_address_link"><?=t( 'Apply Links' )?></div>
    </div>
    <div class="stv_korea_address_list">
        <div class="stv_korea_address_list_message"><?=korea_address_no_search_data()?></div>
    </div>
</div>
