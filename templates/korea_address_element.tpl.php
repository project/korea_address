<?php
/**
 * Created by PhpStorm.
 * User: pig482
 * Date: 2016. 11. 21.
 * Time: PM 5:11
 */
$element = $variables['element'];
?>
<div id="<?=$element['#id']?>" class="stv_korea_address_widget">
    <?php print theme( 'form_element_label', $variables ); ?>
    <div class="stv_korea_address_wrapper">
        <?php print $element['#children']; ?>
    </div>
</div>