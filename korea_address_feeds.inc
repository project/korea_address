<?php
/**
 * Created by PhpStorm.
 * User: pig482
 * Date: 2017. 6. 20.
 * Time: PM 1:51
 */

function korea_address_feeds_processor_targets($entity_type, $bundle) {
    $targets = array();

    foreach( field_info_instances( $entity_type, $bundle ) as $name => $instance ) {
        $info = field_info_field( $name );
        $label = $instance['label'];

        if( $info['type'] == 'stv_korea_address' ) {
            $targets = array(
                "$name:zipcode" => array(
                    'name' => t( "$label:Zipcode" ),
                    'description' => t( 'Zipcode of Korea address' ),
                    'callback' => 'korea_address_import_zipcode_for_feeds'
                ),
                "$name:address" => array(
                    'name' => t( "$label:Address" ),
                    'description' => t( 'Address of Korea address' ),
                    'callback' => 'korea_address_import_address_for_feeds'
                ),
                "$name:address_slave" => array(
                    'name' => t( "$label:Slave Address" ),
                    'description' => t( 'Slave of Korea address' ),
                    'callback' => 'korea_address_import_slaveaddress_for_feeds'
                ),
                "$name:dong_name" => array(
                    'name' => t( "$label:Dong Name" ),
                    'description' => t( 'Dong Name of Korea address' ),
                    'callback' => 'korea_address_import_dong_name_for_feeds'
                ),
                "$name:building_name" => array(
                    'name' => t( "$label:Building Name" ),
                    'description' => t( 'Building Name of Korea address' ),
                    'callback' => 'korea_address_import_building_name_for_feeds'
                )
            );
        }
    }

    return $targets;
}

function korea_address_import_zipcode_for_feeds(FeedsSource $source, $entity, $target, array $values, array $mapping) {
    $t = explode( ':', $target );
    $fieldname = $t[0];

    $entity->{$fieldname}[$entity->language][0]['zipcode'] = reset( $values );
}

function korea_address_import_address_for_feeds(FeedsSource $source, $entity, $target, array $values, array $mapping) {
    $t = explode( ':', $target );
    $fieldname = $t[0];

    $entity->{$fieldname}[$entity->language][0]['address'] = reset( $values );
}

function korea_address_import_slaveaddress_for_feeds(FeedsSource $source, $entity, $target, array $values, array $mapping) {
    $t = explode( ':', $target );
    $fieldname = $t[0];

    $entity->{$fieldname}[$entity->language][0]['address_slave'] = reset( $values );
}

function korea_address_import_dong_name_for_feeds(FeedsSource $source, $entity, $target, array $values, array $mapping) {
    $t = explode( ':', $target );
    $fieldname = $t[0];

    $entity->{$fieldname}[$entity->language][0]['dong_name'] = reset( $values );
}

function korea_address_import_building_name_for_feeds(FeedsSource $source, $entity, $target, array $values, array $mapping) {
    $t = explode( ':', $target );
    $fieldname = $t[0];

    $entity->{$fieldname}[$entity->language][]['building_name'] = reset( $values );
}