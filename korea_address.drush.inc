<?php
/**
 * Created by PhpStorm.
 * User: pig482
 * Date: 2016. 11. 18.
 * Time: PM 3:40
 */

/**
 * @brief  drush command 정보를 리턴합니다
 * @return mixed
 */
function korea_address_drush_command() {
    $items['korea_address_import'] = array(
        'description' => t('Korea Address Import'),
        'aliases' => array( 'ka_import' ),
        'callback' => 'korea_address_drush_import',
        'arguments' => array(
            'file' => 'file'
        )
    );

    $items['korea_address_drop_sido'] = array(
        'description' => t('Korea Address Import'),
        'aliases' => array( 'ka_drop_sido' ),
        'callback' => 'krea_address_drop_all'
    );
    return $items;
}

/**
 * @brief CSV를 통해 광역 자치구의 주소 정보를 테이블에 저장합니다
 * @param string $file
 */
function korea_address_drush_import( $file = '' ) {
    if( empty( $file ) ) {
        drush_log( t( 'You need file path for this function' ), 'error' );
    }
    else {
        $message = korea_address_import_address( $file );
        drush_log( $message['message'], $message['status'] );
    }
}