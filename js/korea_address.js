/**
 * Created by pig482 on 2016. 11. 22..
 */
(function($) {
    /**
     * @brief 주소 입력 폼 위젯을 초기화합니다
     * @type {{attach: Function}}
     */
    Drupal.behaviors.initKorea_address = {
        attach: function( context, settings ) {
            $( '.stv_korea_address_wrapper .korea_address_zipcode_search').click( function() {
                var component_id = $(this).data( 'component_id' );
                Drupal.behaviors.showSearchKoreaAddress( component_id );
            });
        }
    };

    /**
     * @brief 주소 검색챙을 출력합니다
     * @param component_id
     */
    Drupal.behaviors.showSearchKoreaAddress = function( component_id ) {
        $.ajax( {
            url: '/korea_address/search_address_form',
            method: 'POST',
            data: {
                return_id: component_id
            },
            dataType: 'html',
            success: function( data, status, jqXHR ) {
                Drupal.behaviors.appendKoreaAddressSearchForm( data );
            },
            error: function( jqXHR, textStatus, errorThrown ) {
                alert( 'error' );
            }
        } );
    };

    /**
     * @brief 주소 검색창에 표시되는 폼 및 각 컴포넌트에 이벤트 헨들러를 지정합니다
     * @param data
     *
     * @see Drupal.behaviors.showSearchKoreaAddress()
     */
    Drupal.behaviors.appendKoreaAddressSearchForm = function( data ) {
        var html = '<div class="stv_korea_address_search_layer_wrapper">';
        html    += '    <div class="stv_korea_address_background"></div>';
        html    += '    <div class="stv_korea_address_content_wrapper">';
        html    += '        <div class="stv_korea_address_search_form_wrapper">';
        html    += '            <div class="stv_korea_address_search_form_close">';
        html    += '                <p>Close</p>';
        html    += '            </div>';
        html    += data;
        html    += '        </div>';
        html    += '    </div>';
        html    += '</div>';
        $('body').append( html );
        $( '.stv_korea_address_search_form_close' ).click( Drupal.behaviors.closeKoreaAddressSearchForm );
        $( '#stv_korea_address_search_submit' ).click( Drupal.behaviors.searchKoreaAddress );
        $( '#stv_korea_address_load_name').keydown( function( event ) {
            if( event.keyCode == 13 ) {
                event.preventDefault()
                $( '#stv_korea_address_search_submit').trigger( 'click' );
            }
        });
        $( '#stv_korea_address_search_sido').change( Drupal.behaviors.replaceKoreaAddressSIGUGUN );
    };

    /**
     * @brief 주소 검색창을 닥고 해당 DOM을 삭제합니다
     */
    Drupal.behaviors.closeKoreaAddressSearchForm = function() {
        $( '.stv_korea_address_search_layer_wrapper').remove();
    };

    /**
     * @brief 광역 자치구를 선택했을 때 해당 자치구의 시/군/구 정보를 '#stv_korea_address_sigugun' 업데이트합니다
     *
     * @see Drupal.behaviors.appendKoreaAddressSearchForm()
     */
    Drupal.behaviors.replaceKoreaAddressSIGUGUN = function() {
        var sido = $(this).val();
        $.ajax( {
            url: '/korea_address/get_sigugun_list',
            method: 'POST',
            data: {
                sido_table_name: sido
            },
            dataType: 'html',
            success: function( data, status, jqXHR ) {
                $( '#stv_korea_address_sigugun').html( data );
            },
            error: function( jqXHR, textStatus, errorThrown ) {
                alert( 'error' );
            }
        } );
    };

    /**
     * @brief 광역 자치구, 시/군/구, 도로명을 입력받아 주소를 검색하여 목록을 출력합니다.
     *
     * @see Drupal.behaviors.appendKoreaAddressSearchForm()
     */
    Drupal.behaviors.searchKoreaAddress = function() {
        var sido = $( '#stv_korea_address_search_sido' ).val();
        var sigugun = $( '#stv_korea_address_sigugun' ).val();
        var load_name = $( '#stv_korea_address_load_name').val();
        var component_id = $(this).data( 'component_id' );
        $( '.stv_korea_address_list' ).html( '<div class="stv_korea_address_waite_message"><p>' + Drupal.t( 'Searching...') + '</p><p>' + Drupal.t( 'It may take some time. Please wait a moment.' ) + '</p></div>')
        $.ajax( {
            url: '/korea_address/search_korea_address',
            method: 'POST',
            data: {
                sido_table_name: sido,
                sigugun_name: sigugun,
                load_name: load_name,
                return_id: component_id
            },
            dataType: 'html',
            success: function( data, status, jqXHR ) {
                $( '.stv_korea_address_list').html( data );
            },
            error: function( jqXHR, textStatus, errorThrown ) {
                alert( 'error' );
            }
        } );
    };

    /**
     * @brief 주소 목록의 'Select' 링크를 클릭했을 때 호출되는 함수로 해당 주소를 폼에 업데이트 합니다
     * @param componemt_id
     * @param address
     */
    Drupal.behaviors.updateKoreaAddressAtComponent = function( componemt_id, address ) {
        for( name in address ) {
            $( '#' + componemt_id + ' .' + name).val( address[ name ] );
        }
        Drupal.behaviors.closeKoreaAddressSearchForm();
    };
}(jQuery));