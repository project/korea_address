(function($) {
    Drupal.behaviors.initKorea_address_daum = {
        attach: function( context, settings ) {
            $( '.stv_korea_address_daum_wrapper .korea_address_zipcode_search').click( function() {
                var component_id = $(this).data( 'component_id' );
                Drupal.behaviors.execDaumPostcode( component_id );
            });
            $( '.daum_address_search_wrapper .daum_address_list_close > div').click( Drupal.behaviors.HideDaumAddressWrapper );
        }
    };

    Drupal.behaviors.HideDaumPostcode = function( compnent_id ) {
        $( '#' + compnent_id + ' .daum_address_list_wrapper' ).hide();
    };

    Drupal.behaviors.HideDaumAddressWrapper = function() {
        obj = $(this).parent();
        $(obj).parent().hide();
    }

    Drupal.behaviors.ShowDaumPostcode = function( compnent_id ) {
        $( '#' + compnent_id + ' .daum_address_list_wrapper' ).show();
    };

    Drupal.behaviors.execDaumPostcode = function( component_id ) {
        var currentScroll = Math.max(document.body.scrollTop, document.documentElement.scrollTop);
        var component_id_var = component_id;
        var component_wrapper = $( '#' + component_id_var + ' .daum_address_list_wrapper')[0];

        Drupal.behaviors.ShowDaumPostcode( component_id_var );

        new daum.Postcode({
            oncomplete: function(data) {
                // 검색결과 항목을 클릭했을때 실행할 코드를 작성하는 부분.

                // 각 주소의 노출 규칙에 따라 주소를 조합한다.
                // 내려오는 변수가 값이 없는 경우엔 공백('')값을 가지므로, 이를 참고하여 분기 한다.
                var address = data.address; // 최종 주소 변수
                var dong_name = '';
                var building_name = '';

                // 기본 주소가 도로명 타입일때 조합한다.
                if(data.addressType === 'R'){
                    //법정동명이 있을 경우 추가한다.
                    if(data.bname !== ''){
                        dong_name = data.bname;
                    }
                    // 건물명이 있을 경우 추가한다.
                    if(data.buildingName !== ''){
                        building_name = data.buildingName;
                    };
                }

                // 우편번호와 주소 정보를 해당 필드에 넣는다.
                $( '#' + component_id_var + ' .zipcode' ).val( data.zonecode );
                $( '#' + component_id_var + ' .address' ).val( address );
                $( '#' + component_id_var + ' .dong_name' ).val( dong_name );
                $( '#' + component_id_var + ' .building_name' ).val( building_name );

                Drupal.behaviors.HideDaumPostcode( component_id_var );

                // 우편번호 찾기 화면이 보이기 이전으로 scroll 위치를 되돌린다.
                document.body.scrollTop = currentScroll;
            },
            // 우편번호 찾기 화면 크기가 조정되었을때 실행할 코드를 작성하는 부분. iframe을 넣은 element의 높이값을 조정한다.
            onresize : function(size) {
                //component_wrapper.style.height = size.height+'px';
            },
            width : '100%',
            height : '100%'
        }).embed(component_wrapper);
    }
}(jQuery));