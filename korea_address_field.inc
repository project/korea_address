<?php
/**
 * Created by PhpStorm.
 * User: pig482
 * Date: 2016. 11. 21.
 * Time: PM 12:40
 */

/**
 * @brief 필드 정보를 배열로 리턴합니다
 * @return array
 *
 * @see hook_field_info()
 */
function korea_address_field_info() {
    return array(
        'stv_korea_address' => array(
            'label' => t( 'Korea Address' ),
            'description' => t( 'Provide searching Zipcode and fill Address field.' ),
            'default_widget' => 'korea_address',
            'default_formatter' => 'korea_address'
        ),
    );
}

/**
 * @brief 폼 UI를 통해 입력된 값의 유효성을 검사합니다 유효성 검사에서 오류가 발생하면 에러 메시지를 설정합니다
 * @param $entity_type
 * @param $entity
 * @param $field
 * @param $instance
 * @param $langcode
 * @param $items
 * @param $errors
 *
 * @see hook_field_validate()
 */
function korea_address_field_validate( $entity_type, $entity, $field, $instance, $langcode, $items, &$errors ) {
    foreach( $items as $delta => $item ) {
        if( empty( $item['zipcode'] ) && !empty( $item['address' ] ) ) {
            $errors[$field['field_name']][$langcode][$delta][] = array(
                'error' => 'korea_address_zipcode_invalid',
                'message' => t( 'Wrong address input.')
            );
        }
        else if( empty( $item['zipcode'] ) && empty( $item['address'] ) && !empty( $item['address_slave'] ) ) {
            $errors[$field['field_name']][$langcode][$delta][] = array(
                'error' => 'korea_address_zipcode_invalid',
                'message' => t( 'Wrong address input.')
            );
        }
    }
}

/**
 * @brief 입력된 내용이 비었는지를 체크하여 비었으면 TRUE, 그렇지 않으면 FALSE를 리턴합니다
 * @param $item
 * @param $field
 * @return bool
 *
 * @see hook_field_is_empty()
 */
function korea_address_field_is_empty( $item, $field ) {
    return ( empty( $item['zipcode'] ) && empty( $item['address'] ) && empty( $item['address_slave'] ) );
}

/**
 * @breif 필드의 내용 출력형식애 대한 정보를 리턴합니다
 * @return array
 *
 * @see field_formatter_info()
 */
function korea_address_field_formatter_info() {
    return array(
        'korea_address' => array(
            'label' => t( 'Korea Address' ),
            'field types' => array( 'stv_korea_address' )
        )
    );
}

/**
 * @brief 필드의 내용을 출력형식에 맞추어 출력되도록 설정합니다
 * @param $entity_type
 * @param $entity
 * @param $field
 * @param $instance
 * @param $langcode
 * @param $items
 * @param $display
 * @return array
 *
 * @see hook_field_formatter_view()
 */
function korea_address_field_formatter_view( $entity_type, $entity, $field, $instance, $langcode, $items, $display ) {
    $element = array();

    if( $display['type'] == 'korea_address' ) {
        foreach( $items as $delta => $item ) {
            $element[$delta] = array(
                '#type' => 'html_tag',
                '#tag' => 'p',
                '#attributes' => array(
                    'class' => array( 'stv_korea_address' )
                ),
                '#value' => _korea_address_custom_field_format( $item )
            );
        }
    }
    return $element;
}

/**
 * @brief 필드의 내용을 출력형식에 맞추어 조합합니다 이 함수는 korea_address_field_formatter_view() 함수에 의해 호출됩니다
 * @param $item
 * @return string
 *
 * @see korea_address_field_formatter_view();
 * @see hook_field_formatter_view()
 */
function _korea_address_custom_field_format( $item ) {
    $address = $item['address'];
    if( !empty( $item['address_slave'] ) ) $address .= ' ' . $item['address_slave'];
    $build = '';

    if( !empty( $item['dong_name'] ) ) $build = $item['dong_name'];

    if( !empty( $build ) ) $address .= ' (' . $build . ')';
    return $address;
}

/**
 * @brief 필드의 내용을 입력받는 form UI 위젯 정보를 리턴합니다
 * @return array
 *
 * @see hook_field_widget_info()
 */
function korea_address_field_widget_info() {
    return array(
        'korea_address' => array(
            'label' => t( 'Korea Address' ),
            'field types' => array( 'stv_korea_address' )
        ),
        'korea_address_daum' => array(
            'label' => t( 'Korea Address(Daum)'),
            'field types' => array( 'stv_korea_address' )
        )
    );
}

/**
 * @brief 필드 입력에 필요한 form UI를 리턴합니다
 * @param $form
 * @param $form_state
 * @param $field
 * @param $instance
 * @param $langcode
 * @param $items
 * @param $delta
 * @param $element
 * @return array
 *
 * @see hook_field_widget_form()
 */
function korea_address_field_widget_form( &$form, &$form_state, $field, $instance, $langcode, $items, $delta, $element ) {
    $widget = $element;
    $widget['#delta'] = $delta;

    $value = isset( $items[$delta] ) ? $items[$delta] : array( 'zipcode' => '', 'address' => '', 'address_slave' => '', 'dong_name' => '', 'building_name' => '' );

    // 주소필드는 하나의 UI로 만들 수 없어 별도의 커스텀 UI 컴포넌트를 작성하여 지정합니다
    if( $instance['widget']['type'] == 'korea_address' ) {
        $widget += array(
            '#type' => 'korea_address',
            '#default_value' => $value
        );
    }
    else if( $instance['widget']['type'] == 'korea_address_daum' ) {
        $widget += array(
            '#type' => 'korea_address_daum',
            '#default_value' => $value
        );
    }

    $element = $widget;
    return $element;
}

/**
 * @brief 주소 입쳘력관련 UI 출력용 테마 정보를 리턴합니다
 * @return array
 *
 * @see hook_theme()
 */
function korea_address_theme() {
    return array(
        'korea_address_element' => array(
            'render element' => 'element',
            'path' => drupal_get_path( 'module', 'korea_address' ) . '/templates',
            'template' => 'korea_address_element'
        ),
        'korea_address_daum_element' => array(
            'render element' => 'element',
            'path' => drupal_get_path( 'module', 'korea_address' ) . '/templates',
            'template' => 'korea_address_daum_element'
        ),
        'korea_address_search_form' => array(
            'variables' => array(
                'sido' => array(),
                'return_id' => ''
            ),
            'path' => drupal_get_path( 'module', 'korea_address' ) . '/templates',
            'template' => 'korea_address_search_form'
        ),
        'korea_address_address_list' => array(
            'variables' => array(
                'address' => FALSE,
                'return_id' => ''
            ),
            'path' => drupal_get_path( 'module', 'korea_address' ) . '/templates',
            'template' => 'korea_address_address_list'
        )
    );
}

/**
 * @brief 주소 입력을 위한 커스텀 컴포넌트 타입을 리턴합니다.
 * @return array
 *
 * @see hook_element_info()
 */
function korea_address_element_info() {
    return array(
        'korea_address' => array(
            '#input' => TRUE,
            '#tree' => TRUE,
            '#autocomplete_path' => FALSE,
            '#process' => array( '_korea_address_element_process' ),
            '#theme_wrappers' => array('korea_address_element'),
            '#value_callback' => '_korea_address_value_callback',
            '#attached' => array(
                'css' => array(
                    drupal_get_path( 'module', 'korea_address' ) . '/css/korea_address.css'
                ),
                'js' => array(
                    drupal_get_path( 'module', 'korea_address' ) . '/js/korea_address.js'
                )
            )
        ),
        'korea_address_daum' => array(
            '#input' => TRUE,
            '#tree' => TRUE,
            '#autocomplete_path' => FALSE,
            '#process' => array( '_korea_address_element_process' ),
            '#theme_wrappers' => array('korea_address_daum_element'),
            '#value_callback' => '_korea_address_value_callback',
            '#attached' => array(
                'css' => array(
                    drupal_get_path( 'module', 'korea_address' ) . '/css/korea_address.css'
                ),
                'js' => array(
                    'https://ssl.daumcdn.net/dmaps/map_js_init/postcode.v2.js',
                    drupal_get_path( 'module', 'korea_address' ) . '/js/korea_address_daum.js'
                )
            )
        )
    );
}

/**
 * @brief 컴포넌트의 상세 UI를 리턴합니다
 * @param $element
 * @param $form_state
 * @param $complete_form
 * @return mixed
 *
 * @see korea_address_element_info()
 */
function _korea_address_element_process( $element, $form_state, $complete_form ) {
    if( !isset( $element['#default_value'] ) ) {
        $element['#default_value'] = array(
            'zipcode' => '',
            'address' => '',
            'address_slave' => '',
            'dong_name' => '',
            'building_name' => ''
        );
    }
    $element['zipcode'] = array(
        '#type' => 'textfield',
        '#size' => 7,
        '#title_display' => 'invisible',
        '#attributes' => array(
            'class' => array( 'zipcode_box', 'zipcode' ),
            'readonly' => 'readonly'
        ),
        '#default_value' => $element['#default_value']['zipcode']
    );
    $element['search_zipcode'] = array(
        '#type' => 'markup',
        '#markup' => '<input type="button" data-component_id="' . $element['#id'] . '" class="korea_address_zipcode_search form-submit" value="' . t( 'Search zip code' ) . '"/>'
    );
    if( $element['#type'] == 'korea_address_daum' ) {
        $element['daum_address_list_wrapper'] = array(
            '#type' => 'markup',
            '#markup' => '<div class="daum_address_search_wrapper"><div class="daum_address_list_wrapper"><div class="daum_address_list_close"><div>X</div></div></div></div>',
        );
    }
    $element['address'] = array(
        '#type' => 'textfield',
        '#size' => 60,
        '#max_length' => 255,
        '#title' => t( 'Address' ),
        '#attributes' => array(
            'class' => array( 'address' ),
            'readonly' => 'readonly',
            'placeholder' => t( 'Search Address.' )
        ),
        '#default_value' => $element['#default_value']['address']
    );
    $element['address_slave'] = array(
        '#type' => 'textfield',
        '#size' => 60,
        '#max_length' => 255,
        '#title' => t( 'Addtional Address' ),
        '#attributes' => array(
            'class' => array( 'address_slave' ),
            'placeholder' => t('Enter the Additional Address.' )
        ),
        '#default_value' => $element['#default_value']['address_slave']
    );
    $element['dong_name'] = array(
        '#type' => 'hidden',
        '#attributes' => array(
            'id' => $element['#id'] . '-dong_name',
            'class' => array( 'dong_name')
        ),
        '#default_value' => $element['#default_value']['dong_name']
    );
    $element['building_name'] = array(
        '#type' => 'hidden',
        '#attributes' => array(
            'id' => $element['#id'] . '-building_name',
            'class' => array( 'building_name' )
        ),
        '#default_value' => $element['#default_value']['building_name']
    );

    return $element;
}

function _korea_address_value_callback( $element, $input = array(), &$form_state ) {
    if( !empty( $input['zipcode'] ) && !empty( $input['address'] ) ) {
        return $input;
    }
    else {
        $input = array();
        return $input;
    }
}