Korea Adress
=============
This module is a field module that provides the address input interface used in Korea.
It also provides related functions for importing content using the Feeds module.

Installation
-------------
If you use the address information after saving it in your own database,
you have to download it from EPost (https://epost.go.kr/search/zipcode/areacdAddressDown.jsp)
and update the data using Drush command.
If you use the 'Korea Address' widget, you must store the address information in its own database.

Address information database construction
------------------------------------------
1. Visit download page(https://epost.go.kr/search/zipcode/areacdAddressDown.jsp)
2. Download the compressed file by clicking the '받기' button on the right side of '지역별 주소 DB' in
   the '고시파일 내려받기' list.
3. Unzip the downloaded file.
4. Move to the Site Router folder where the module is installed.
5. Use the Drush command to import the '~ .txt' file into the database.
   Ex) drush korea_address_import "path_to_unzipfiles/서울특별시.txt"
   Ex) drush ka_import "path_to_unzipfiles/서울특별시.txt"

Support widget
---------------
This module provides 'Korea Address' widget and 'Korea Address (Daum)' widget.
The 'Korea Address' widget uses its own database. 'Korea Address (Daum)' uses external data,
so no additional work is required.

Integration with Feeds module
------------------------------
Sets the relationship between the CSV file header and each element of the address field.

1. Go to the 'Structure > Feeds importer' page in the Admin menu.
2. Click the 'Add importer' link at the top of the screen.
3. From 'Basic settings', click the various settings link of 'Processor' to set up to import data of CSV file.
4. Finally, click the 'Mapping' link to set up relationships with each element in the field.
   '우편번호(zipcode)' -> '~:zipcode'
   '주소(address)' -> '~:address'
   '보조주소(address_slave)' -> '~:address_slave'
   '동명(dong_name)' -> '~:dong_name'
   '건물명(building_name)' '~:building_name'
